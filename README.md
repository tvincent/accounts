## Use the Issues menu on the left to file tickets for this project.

### When creating a new ticket, you can choose a template to either:

 * Create a new account on Debian Social services
 * Reset a password
 * Create a WordPress instance for either yourself or a team on wordpress.debian.social