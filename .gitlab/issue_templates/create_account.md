Please create an account for me on the following services:

(delete all that don't apply)

 - pixelfed.debian.social
 - peertube.debian.social
 - pleroma.debian.social
 - wordpress.debian.social
 - wirtefreely.debian.social
 
 I understand that this services are in beta state and may still have some
 issues.
 
 I agree to uphold Debian's community standards, including the Debian code
 of Conduct, when using these services.

/label account
/cc @jcc @paddatrapper @rhonda
