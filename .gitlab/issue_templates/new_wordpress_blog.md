Delete which doesn't apply:

Please create a new wordpress.debian.social blog for [myself|a team].

The team name is: [team name]

I agree to uphold Debian's community standards, including the Debian code
of Conduct, when using these services.

/label account
/cc @jcc @paddatrapper @rhonda